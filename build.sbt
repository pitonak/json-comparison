name         := "json-comparison"
organization := "io.github.piton4k"

scalaVersion := "2.12.10"

val versions = new {
  val circe                 = "0.12.2"
  val circeDerivation       = "0.12.0-M7"
  val play                  = "2.7.4"
  val playJsonDerivedCodecs = "6.0.0"
  val json4s                = "3.6.7"
  val fastparse             = "2.1.3"
  val sourcecode            = "0.1.7"
  val fansi                 = "0.2.7"
  val log4s                 = "1.8.2"
  val logback               = "1.2.3"
  val scalatest             = "3.0.8"
}

libraryDependencies ++= Seq(
  "io.circe"              %% "circe-core"           % versions.circe,
  "io.circe"              %% "circe-generic"        % versions.circe,
  "io.circe"              %% "circe-generic-extras" % versions.circe,
  "io.circe"              %% "circe-parser"         % versions.circe,
  "io.circe"              %% "circe-literal"        % versions.circe,
  "io.circe"              %% "circe-derivation"     % versions.circeDerivation,

  "com.typesafe.play"     %% "play-json"            % versions.play,
  "org.julienrf"          %% "play-json-derived-codecs" % versions.playJsonDerivedCodecs,

  "org.json4s"            %% "json4s-native"        % versions.json4s,
  "org.json4s"            %% "json4s-ext"           % versions.json4s,

  "com.lihaoyi"           %% "fastparse"            % versions.fastparse,
  "com.lihaoyi"           %% "sourcecode"           % versions.sourcecode,
  "com.lihaoyi"           %% "fansi"                % versions.fansi,
  "org.log4s"             %% "log4s"                % versions.log4s,
  "ch.qos.logback"        %  "logback-classic"      % versions.logback,
  "org.scalatest"         %% "scalatest"            % versions.scalatest % Test,
)

addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.11.0" cross CrossVersion.full)
addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.1")
addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.1" cross CrossVersion.full)

scalacOptions ++= Seq(
  "-deprecation",                      // Emit warning and location for usages of deprecated APIs.
  "-encoding", "utf-8",                // Specify character encoding used by source files.
  "-explaintypes",                     // Explain type errors in more detail.
  "-feature",                          // Emit warning and location for usages of features that should be imported explicitly.
  "-language:existentials",            // Existential types (besides wildcard types) can be written and inferred
  "-language:experimental.macros",     // Allow macro definition (besides implementation and application)
  "-language:higherKinds",             // Allow higher-kinded types
  "-language:implicitConversions",     // Allow definition of implicit functions called views
  "-unchecked",                        // Enable additional warnings where generated code depends on assumptions.
  "-Xcheckinit",                       // Wrap field accessors to throw an exception on uninitialized access.
  //  "-Xfatal-warnings",                  // Fail the compilation if there are any warnings.
  "-Xlint:adapted-args",               // Warn if an argument list is modified to match the receiver.
  "-Xlint:constant",                   // Evaluation of a constant arithmetic expression results in an error.
  "-Xlint:delayedinit-select",         // Selecting member of DelayedInit.
  "-Xlint:doc-detached",               // A Scaladoc comment appears to be detached from its element.
  "-Xlint:inaccessible",               // Warn about inaccessible types in method signatures.
  "-Xlint:infer-any",                  // Warn when a type argument is inferred to be `Any`.
  "-Xlint:missing-interpolator",       // A string literal appears to be missing an interpolator id.
  "-Xlint:nullary-override",           // Warn when non-nullary `def f()' overrides nullary `def f'.
  "-Xlint:nullary-unit",               // Warn when nullary methods return Unit.
  "-Xlint:option-implicit",            // Option.apply used implicit view.
  "-Xlint:package-object-classes",     // Class or object defined in package object.
  "-Xlint:poly-implicit-overload",     // Parameterized overloaded implicit methods are not visible as view bounds.
  "-Xlint:private-shadow",             // A private field (or class parameter) shadows a superclass field.
  "-Xlint:stars-align",                // Pattern sequence wildcard must align with sequence component.
  "-Xlint:type-parameter-shadow",      // A local type parameter shadows a type already in scope.
  "-Ywarn-dead-code",                  // Warn when dead code is identified.
  "-Ywarn-extra-implicit",             // Warn when more than one implicit parameter section is defined.
  "-Ywarn-numeric-widen",              // Warn when numerics are widened.
  "-Ywarn-unused:implicits",           // Warn if an implicit parameter is unused.
  "-Ywarn-unused:imports",             // Warn if an import selector is not referenced.
  "-Ywarn-unused:locals",              // Warn if a local definition is unused.
  "-Ywarn-unused:params",              // Warn if a value parameter is unused.
  "-Ywarn-unused:patvars",             // Warn if a variable bound in a pattern is unused.
  "-Ywarn-unused:privates",            // Warn if a private member is unused.
  "-Ywarn-value-discard",              // Warn when non-Unit expression results are unused.
)
