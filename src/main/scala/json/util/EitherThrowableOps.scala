package json.util

class EitherThrowableOps[A](private val self: Either[Throwable, A]) extends AnyVal {
  def get: A = self match {
    case Left(error) => throw error
    case Right(value) => value
  }
}

trait EitherThrowableSyntax {
  implicit def toEitherThrowableOps[A](self: Either[Throwable, A]): EitherThrowableOps[A] =
    new EitherThrowableOps(self)
}
