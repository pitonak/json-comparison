package json.util

import json.@@

class TagOps[A](private val self: A) extends AnyVal {
  @inline def tag[T]: A @@ T = self.asInstanceOf[A @@ T]
}

trait TagSyntax {
  implicit def toTagOps[A](self: A): TagOps[A] =
    new TagOps(self)
}

class TaggedOps[A, T](private val self: A @@ T) extends AnyVal {
  @inline def untag: A = self.asInstanceOf[A]
}

trait TaggedSyntax {
  implicit def toTaggedOps[A, T](self: A @@ T): TaggedOps[A, T] =
    new TaggedOps(self)
}
