package json

import java.time.Instant

import io.circe._
import io.circe.derivation._
import json.App.Example
import org.json4s.{DefaultFormats, Formats}
import org.json4s.native.JsonMethods
import play.api.libs.json.{Json => PlayJson, _}

import scala.util.Random

object DefaultArgs extends Example {
  case class Data(
    id: Int = Random.nextInt(100),
    createdAt: Instant = Instant.now()
  )

  implicit val formats: Formats = DefaultFormats

  implicit val codec: Codec[Data] = deriveCodec[Data]((n: String) => n, true, None)

  implicit val format: OFormat[Data] =
    PlayJson.configured[PlayJson.WithDefaultValues].format[Data]

  def run(): Unit = {
    val rawValue = "{}"

    test("json4s") {
      val json4sObj = JsonMethods.parse(rawValue)
      println(json4sObj.extract[Data])
      println(json4sObj.extract[Data])
    }

    test("circe") {
      val circeObj = parser.parse(rawValue).get
      println(circeObj.as[Data].get)
      println(circeObj.as[Data].get)
    }

    test("play-json") {
      val playObj = PlayJson.parse(rawValue)
      println(playObj.as[Data])
      println(playObj.as[Data])
    }
  }
}
