package json

import io.circe._
import io.circe.generic.semiauto._
import json.AnyValNewtype.{idCodec, idFormat}
import json.App.Example
import org.json4s.{DefaultFormats, Formats}
import org.json4s.native.JsonMethods
import play.api.libs.json.{Json => PlayJson, _}

object Generics extends Example {
  case class Data[A](
    total: Int,
    values: List[A]
  )

  implicit val formats: Formats = DefaultFormats

  implicit def codec[A: Encoder: Decoder]: Codec[Data[A]] =
    deriveCodec[Data[A]]

  implicit def format[A: Format]: OFormat[Data[A]] =
    PlayJson.format[Data[A]]

  def run(): Unit = {
    val rawValue =
      """{
        |  "total": 10,
        |  "values": [
        |    "hello",
        |    "there"
        |  ]
        |}""".stripMargin

    test("json4s") {
      val json4sObj = JsonMethods.parse(rawValue)
      println(json4sObj.extract[Data[Id]])
    }

    test("circe") {
      val circeObj = parser.parse(rawValue).get
      println(circeObj.as[Data[Id]].get)
    }

    test("play-json") {
      val playObj = PlayJson.parse(rawValue)
      println(playObj.as[Data[Id]])
    }
  }
}
