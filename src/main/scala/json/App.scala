package json

object App {
  def main(args: Array[String]): Unit = {
    val examples = List(
      AnyValNewtype,
      TaggedNewtype,
      DefaultArgs,
      ManyArgs,
      Adts,
      Generics
    )

    examples.foreach { example =>
      val label = example.getClass.getSimpleName.stripSuffix("$")
      println("=" * label.length)
      println(label)
      println("=" * label.length)
      println()
      example.run()
      println()
      println()
    }
  }

  trait Example {
    def main(args: Array[String]): Unit = run()
    def run(): Unit
  }
}
