package json

import io.circe._
import io.circe.generic.semiauto._
import json.App.Example
import org.json4s.{DefaultFormats, Formats}
import org.json4s.native.JsonMethods
import play.api.libs.json.{Json => PlayJson, _}

object ManyArgs extends Example {
  case class Data(
    arg1: String,
    arg2: String,
    arg3: String,
    arg4: String,
    arg5: String,
    arg6: String,
    arg7: String,
    arg8: String,
    arg9: String,
    arg10: String,
    arg11: String,
    arg12: String,
    arg13: String,
    arg14: String,
    arg15: String,
    arg16: String,
    arg17: String,
    arg18: String,
    arg19: String,
    arg20: String,
    arg21: String,
    arg22: String,
    arg23: String,
    arg24: String,
    arg25: String,
    arg26: String,
    arg27: String,
    arg28: String,
    arg29: String,
    arg30: String
  )

  implicit val formats: Formats = DefaultFormats

  implicit val codec: Codec[Data] =
    deriveCodec[Data]

  //  implicit val format: OFormat[Data] =
  //    PlayJson.format[Data]
  implicit val format: Reads[Data] = {
    import play.api.libs.functional.syntax._
    val first = (
      (__ \ "arg1").read[String] and
        (__ \ "arg2").read[String] and
        (__ \ "arg3").read[String] and
        (__ \ "arg4").read[String] and
        (__ \ "arg5").read[String] and
        (__ \ "arg6").read[String] and
        (__ \ "arg7").read[String] and
        (__ \ "arg8").read[String] and
        (__ \ "arg9").read[String] and
        (__ \ "arg10").read[String] and
        (__ \ "arg11").read[String] and
        (__ \ "arg12").read[String] and
        (__ \ "arg13").read[String] and
        (__ \ "arg14").read[String] and
        (__ \ "arg15").read[String] and
        (__ \ "arg16").read[String] and
        (__ \ "arg17").read[String] and
        (__ \ "arg18").read[String] and
        (__ \ "arg19").read[String] and
        (__ \ "arg20").read[String]
      )((a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t) => (a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t))
    val second = (
      (__ \ "arg21").read[String] and
        (__ \ "arg22").read[String] and
        (__ \ "arg23").read[String] and
        (__ \ "arg24").read[String] and
        (__ \ "arg25").read[String] and
        (__ \ "arg26").read[String] and
        (__ \ "arg27").read[String] and
        (__ \ "arg28").read[String] and
        (__ \ "arg29").read[String] and
        (__ \ "arg30").read[String]
      )((a, b, c, d, e, f, g, h, i, j) => (a, b, c, d, e, f, g, h, i, j))

    (first and second) { (a, b) =>
      Data(
        a._1,
        a._2,
        a._3,
        a._4,
        a._5,
        a._6,
        a._7,
        a._8,
        a._9,
        a._10,
        a._11,
        a._12,
        a._13,
        a._14,
        a._15,
        a._16,
        a._17,
        a._18,
        a._19,
        a._20,
        b._1,
        b._2,
        b._3,
        b._4,
        b._5,
        b._6,
        b._7,
        b._8,
        b._9,
        b._10
      )
    }
  }


  def run(): Unit = {
    val rawValue =
      """{
        |  "arg1": "1",
        |  "arg2": "2",
        |  "arg3": "3",
        |  "arg4": "4",
        |  "arg5": "5",
        |  "arg6": "6",
        |  "arg7": "7",
        |  "arg8": "8",
        |  "arg9": "9",
        |  "arg10": "10",
        |  "arg11": "11",
        |  "arg12": "12",
        |  "arg13": "13",
        |  "arg14": "14",
        |  "arg15": "15",
        |  "arg16": "16",
        |  "arg17": "17",
        |  "arg18": "18",
        |  "arg19": "19",
        |  "arg20": "20",
        |  "arg21": "21",
        |  "arg22": "22",
        |  "arg23": "23",
        |  "arg24": "24",
        |  "arg25": "25",
        |  "arg26": "26",
        |  "arg27": "27",
        |  "arg28": "28",
        |  "arg29": "29",
        |  "arg30": "30"
        |}""".stripMargin

    test("json4s") {
      val json4sObj = JsonMethods.parse(rawValue)
      println(json4sObj.extract[Data])
    }

    test("circe") {
      val circeObj = parser.parse(rawValue).get
      println(circeObj.as[Data].get)
    }

    test("play-json") {
      val playObj = PlayJson.parse(rawValue)
      println(playObj.as[Data])
    }
  }
}
