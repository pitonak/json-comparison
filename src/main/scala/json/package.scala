import cats.Invariant
import io.circe._
import json.util._

import scala.util.control.NonFatal

package object json
  extends EitherThrowableSyntax
    with TagSyntax
    with TaggedSyntax {

  type @@[A, T] = {type Self = A; type Tag = T}

  val discriminator = "type"

  def codec[A: Encoder: Decoder]: Codec[A] = Codec.from(Decoder[A], Encoder[A])

  def test(label: String)(block: => Any): Unit = {
    attempt {
      println(label)
      block
    }
  }

  def attempt(block: => Any): Unit = {
    try { block; }
    catch {
      case NonFatal(e) => e.printStackTrace()
    }
  }

  implicit def invariantCodec: Invariant[Codec] = new Invariant[Codec] {
    def imap[A, B](fa: Codec[A])(f: A => B)(g: B => A): Codec[B] = Codec.from(fa.map(f), fa.contramap(g))
  }
}
