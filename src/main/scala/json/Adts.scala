package json

import io.circe._
import io.circe.generic.extras.semiauto.deriveConfiguredCodec
import io.circe.generic.extras.Configuration
import io.circe.generic.semiauto.deriveCodec
import io.circe.generic.JsonCodec
import io.circe.syntax._
import json.Adts.ContactInfo.{Address, PhoneNumber}
import json.App.Example
import julienrf.json.derived
import org.json4s.{DefaultFormats, Extraction, Formats, JValue, Serializer, TypeInfo}
import org.json4s.native.{JsonMethods, Serialization}
import org.json4s.JsonAST.{JObject, JString}
import play.api.libs.json.{__, OFormat, Json => PlayJson}

object Adts extends Example {
  case class Data(contactInfo: List[ContactInfo])

  sealed trait ContactInfo
  object ContactInfo {
    case class PhoneNumber(value: String) extends ContactInfo
    case class Address(
      street: String,
      city: String,
      country: String
    ) extends ContactInfo
  }

  implicit val formats: Formats = DefaultFormats + ContactInfoSerializer

  implicit val configuration: Configuration = Configuration.default.withDiscriminator(discriminator)
  implicit val contactInfoCodec: Codec[ContactInfo] =
    deriveConfiguredCodec[ContactInfo]
  implicit val dataCodec: Codec[Data] =
    deriveCodec[Data]

  implicit val contactInfoFormat: OFormat[ContactInfo] =
    derived.flat.oformat((__ \ discriminator).format[String])
  implicit val dataFormat: OFormat[Data] =
    PlayJson.format[Data]

  def run(): Unit = {
    val rawValue =
      """{
        |  "contactInfo": [
        |    {
        |      "type": "PhoneNumber",
        |      "value": "123456"
        |    },
        |    {
        |      "type": "PhoneNumber",
        |      "value": "678654"
        |    },
        |    {
        |      "type": "Address",
        |      "street": "221b Baker St",
        |      "city": "London",
        |      "country": "United Kingdom"
        |    }
        |  ]
        |}""".stripMargin

    test("json4s") {
      val json4sObj = JsonMethods.parse(rawValue)
      val json4sData = json4sObj.extract[Data]
      println(json4sData)
      val json4sString = Serialization.write(json4sData)
      println(json4sString)
      println(JsonMethods.parse(json4sString).extract[Data] == json4sData)
    }

    test("circe") {
      val circeObj = parser.parse(rawValue).get
      val circeData = circeObj.as[Data].get
      println(circeData)
      val circeString = circeData.asJson.noSpaces
      println(circeString)
      println(parser.parse(circeString).get.as[Data].get == circeData)
    }

    test("play-json") {
      val playObj = PlayJson.parse(rawValue)
      val playJsonData = playObj.as[Data]
      println(playJsonData)
      val playJsonString = PlayJson.toJson(playJsonData).toString
      println(playJsonString)
      println(PlayJson.parse(playJsonString).as[Data] == playJsonData)
    }
  }
}

object ContactInfoSerializer extends Serializer[Adts.ContactInfo] {
  private val ciClass = classOf[Adts.ContactInfo]

  def serialize(implicit format: Formats): PartialFunction[Any, JValue] = {
    case phoneNumber: PhoneNumber =>
      Extraction.decompose(phoneNumber)(DefaultFormats).merge(JObject(discriminator -> JString("PhoneNumber")))
    case address: Address =>
      Extraction.decompose(address)(DefaultFormats).merge(JObject(discriminator -> JString("Address")))
  }

  def deserialize(implicit format: Formats): PartialFunction[(TypeInfo, JValue), Adts.ContactInfo] = {
    case (TypeInfo(`ciClass`, _), value) =>
      (value \ discriminator).extract[String] match {
        case "PhoneNumber" => value.extract[PhoneNumber]
        case "Address" => value.extract[Address]
      }
  }
}
