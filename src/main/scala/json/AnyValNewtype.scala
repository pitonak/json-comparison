package json

import io.circe._
import io.circe.generic.extras.semiauto.deriveUnwrappedCodec
import io.circe.generic.semiauto._
import io.circe.syntax._
import json.App.Example
import org.json4s.{DefaultFormats, Formats}
import org.json4s.native.{JsonMethods, Serialization}
import play.api.libs.functional.syntax._
import play.api.libs.json.{Json => PlayJson, _}

case class Id(value: String) extends AnyVal

object AnyValNewtype extends Example {
  case class Data(
    id: Id,
    children: List[Id]
  )

  implicit val formats: Formats = DefaultFormats

  implicit val idCodec: Codec[Id] = deriveUnwrappedCodec[Id]
  implicit val dataCodec: Codec[Data] =
    deriveCodec[Data]

  implicit val idFormat: Format[Id] = implicitly[Format[String]].inmap[Id](Id, _.value)
  implicit val dataFormat: OFormat[Data] =
    PlayJson.format[Data]

  def run(): Unit = {
    val rawValue =
      """{
        |  "id": "5d41402abc4b2a76b9719d911017c592",
        |  "children": [
        |    "958153f1b8b96ec4c4eb2147429105d9",
        |    "b0be1e547f7a5ee3ffd555428b019d9c"
        |  ]
        |}""".stripMargin

    test("json4s") {
      val json4sObj = JsonMethods.parse(rawValue)
      val json4sData = json4sObj.extract[Data]
      println(json4sData)
      println(Serialization.write(json4sData))
    }

    test("circe") {
      val circeData = parser.decode[Data](rawValue).get
      println(circeData)
      println(circeData.asJson.noSpaces)
    }

    test("play-json") {
      val playData = PlayJson.parse(rawValue).as[Data]
      println(playData)
      println(PlayJson.toJson(playData).toString)
    }
  }
}
