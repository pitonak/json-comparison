package json

import io.circe._
import io.circe.generic.semiauto.deriveCodec
import json.App.Example
import org.json4s.{DefaultFormats, Formats}
import org.json4s.native.JsonMethods
import play.api.libs.json.{OFormat, Json => PlayJson}

object ErrorMessages extends Example {
  case class Data(
    name: String,
    age: Int,
    contactInfo: List[ContactInfo]
  )

  case class ContactInfo(
    phoneNumber: String,
    address: String
  )

  implicit val formats: Formats = DefaultFormats

  implicit val contactInfoCodec: Codec[ContactInfo] =
    deriveCodec[ContactInfo]
  implicit val dataCodec: Codec[Data] =
    deriveCodec[Data]

  implicit val contactInfoFormat: OFormat[ContactInfo] =
    PlayJson.format[ContactInfo]
  implicit val dataFormat: OFormat[Data] =
    PlayJson.format[Data]

  def run(): Unit = {
    val rawValue =
      """{
        |  "firstName": "Luke",
        |  "lastName": "Skywalker",
        |  "age": "19",
        |  "contactInfo": [{
        |    "phoneNumber": 123456
        |  }]
        |}""".stripMargin

    test("json4s") {
      val json4sObj = JsonMethods.parse(rawValue)
      println(json4sObj.extract[Data])
    }

    test("circe") {
      println(parser.decodeAccumulating[Data](rawValue))
    }

    test("play-json") {
      val playObj = PlayJson.parse(rawValue)
      println(playObj.validate[Data])
    }
  }
}
